SKA = SKA || {};

SKA.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "res/img/example.png"},
	 */
	
    //Screen elements
    { name: "title_bg",     type: "image", src: "res/img/title_bg.png" },
    { name: "play",         type: "image", src: "res/img/play.png" },
    { name: "credits",      type: "image", src: "res/img/credits.png" },
    { name: "title_select", type: "image", src: "res/img/title_select.png" },
    { name: "credits_bg", type: "image", src: "res/img/credits_bg.png" },
    { name: "game_over", type: "image", src: "res/img/game_over.png" },
    { name: "game_end", type: "image", src: "res/img/game_end.png" },
    { name: "fish_big", type: "image", src: "res/img/fish_big.png" },
    { name: "font32white",  type: "image", src: "res/img/font32white.png" },
    { name: "font32orange", type: "image", src: "res/img/font32orange.png" },

    //level start elements
    { name: "level1_bg", type: "image", src: "res/img/level1_bg.png" },
    { name: "level1_slide", type: "image", src: "res/img/level1_slide.png" },
    { name: "level1_snap1", type: "image", src: "res/img/level1_snap1.png" },
    { name: "level1_snap2", type: "image", src: "res/img/level1_snap2.png" },
    { name: "level1_snap3", type: "image", src: "res/img/level1_snap3.png" },
    { name: "level2_bg", type: "image", src: "res/img/level2_bg.png" },
    { name: "level2_slide", type: "image", src: "res/img/level2_slide.png" },
    { name: "level2_snap1", type: "image", src: "res/img/level2_snap1.png" },
    { name: "level2_snap2", type: "image", src: "res/img/level2_snap2.png" },
    { name: "level2_snap3", type: "image", src: "res/img/level2_snap3.png" },
    { name: "level3_bg", type: "image", src: "res/img/level3_bg.png" },
    { name: "level3_slide", type: "image", src: "res/img/level3_slide.png" },
    { name: "level3_snap1", type: "image", src: "res/img/level3_snap1.png" },
    { name: "level3_snap2", type: "image", src: "res/img/level3_snap2.png" },
    { name: "level3_snap3", type: "image", src: "res/img/level3_snap3.png" },

    //game objects
    { name: "fish", type: "image", src: "res/img/fish.png" },
    { name: "particle", type: "image", src: "res/img/fish.png" },
    { name: "projectile", type: "image", src: "res/img/projectile.png" },
    { name: "enemy_projectile", type: "image", src: "res/img/enemy_projectile.png" },
    { name: "big_shot", type: "image", src: "res/img/big_shot.png" },

    //enemies
    { name: "jumper", type: "image", src: "res/img/jumper.png" },
    { name: "hopper", type: "image", src: "res/img/hopper.png" },
    { name: "skipper", type: "image", src: "res/img/skipper.png" },
    { name: "buzzer", type: "image", src: "res/img/buzzer.png" },
    { name: "hive_toad", type: "image", src: "res/img/hive_toad.png" },
    { name: "swimmer", type: "image", src: "res/img/swimmer.png" },

    //map images
    { name: "trees", type: "image", src: "res/img/trees.png" },
    { name: "trees2", type: "image", src: "res/img/trees2.png" },
    { name: "trees3", type: "image", src: "res/img/trees3.png" },

    { name: "forest_bg", type: "image", src: "res/img/forest_bg.png" },
    { name: "forest", type: "image", src: "res/img/forest.png" },
    { name: "forest2", type: "image", src: "res/img/forest2.png" },
    { name: "forest3", type: "image", src: "res/img/forest3.png" },
    { name: "bg2", type: "image", src: "res/img/bg2.png" },

    { name: "water_bg", type: "image", src: "res/img/water_bg.png" },
    { name: "water1", type: "image", src: "res/img/water1.png" },
    { name: "water2", type: "image", src: "res/img/water2.png" },
    { name: "water3", type: "image", src: "res/img/water3.png" },
    { name: "water4", type: "image", src: "res/img/water4.png" },


    { name: "metatiles32x32", type: "image", src: "res/img/metatiles32x32.png" },
    { name: "tile", type: "image", src: "res/img/tile.png" },


	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "res/map/example01.tmx"},
 	 */
     { name: "level_1", type: "tmx", src: "res/level_1.tmx" },
     { name: "level_2", type: "tmx", src: "res/level_2.tmx" },
     { name: "level_3", type: "tmx", src: "res/level_3.tmx" },

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "res/sfx/", channel : 1},
	 */	
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "res/sfx/", channel : 2}
	 */
];
