SKA = SKA || {};

SKA.PlayScreen = me.ScreenObject.extend({
    /**     
    *  action to perform on state change
    */
    onResetEvent: function ()
    {
        me.levelDirector.loadLevel("level_" + SKA.level);
    },


    /**     
	 *  action to perform when leaving this screen (state change)
	 */
    onDestroyEvent: function ()
    {
        //me.game.disableHUD();
        me.game.removeAll();
    }
});
