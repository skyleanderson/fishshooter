//namespace initialization
SKA = SKA || {};

//declare any globals
SKA.CONSTANTS = {
    ANIMATION: 4000,

    BASE_GRAVITY: 0.95,
    WATER_GRAVITY: 0.55,

    PLAYER_SPEED: 4.8,
    PLAYER_JUMP: -15,
    PLAYER_PROJECTILE_SPEED: 9.6,
    PROJECTILE_OFFSET_X: 54,
    PROJECTILE_OFFSET_Y: 30,
    PLAYER_SHOT_COOLDOWN: 100,
    SHOT_DAMAGE: 10,
    PLAYER_CHARGE: 1400,
    CHARGE_PARTICLE_RADIUS: 60,
    CHARGE_OFFSET_Y: 16,
    CHARGE_DAMAGE: 2,
    PLAYER_MAX_HEALTH: 100,
    PLAYER_DAMAGE_COOLDOWN: 45,
    PLAYER_BOUNCE_BACK: 15,

    JUMPER: {
        JUMP_COOLDOWN: 1000,
        SHOT_COOLDOWN: 200,
        VELOCITY: -22,
        MAX_HEALTH: 30
    },

    HOPPER: {
        JUMP_COOLDOWN: 500,
        SHOT_COOLDOWN: 2600,
        JUMP_SPEED_Y: -13,
        JUMP_SPEED_X: 3.5,
        SHOT_SPEED: 3,
        MAX_HEALTH: 30,
    },

    SKIPPER: {
        JUMP_COOLDOWN: 1500,
        SHOT_COOLDOWN: 2200,
        JUMP_SPEED_Y: -19,
        JUMP_SPEED_X: 1.4,
        MAX_HEALTH: 80,
        SHOT_SPEED: 5,
        BOSS_SHOT_COOLDOWN: 200,
        BOSS_ANGLE_MAX: 1 * Math.PI,
        BOSS_ANGLE_MIN: .8 * Math.PI,
    },

    BUZZER: {
        SHOT_COOLDOWN: 3500,
        SHOT_SPEED: 3,
        RADIUS_MIN: 30,
        RADIUS_MAX: 80,
        ROTATION_RATE: .05,
        MAX_HEALTH: 15,
        VELOCITY: 3,
    },

    HIVE_TOAD: {
        FULL_COOLDOWN: 1000,
        SHORT_COOLDOWN: 400,
        MAX_HEALTH: 150,
        BUZZERS_MAX: 5
    },

    SWIMMER: {
        FULL_COOLDOWN: 1000,
        SHORT_COOLDOWN: 400,
        SHOT_ROTATION: Math.PI / 3,
        VELOCITY: 1.5,
        Y_VARIANCE: 16,
        ATTACK_RANGE: 50,
        SHOTS: 3,
        MAX_HEALTH: 30,
        SHOT_SPEED: 5
    },
};

SKA.LAYERS = {
    PLAYER: 9,
    PARTICLES: 10,
};

SKA.isPaused = false;

SKA.animationCooldown = SKA.CONSTANTS.ANIMATION;
SKA.bg2 = true;

SKA.PlayerEntity = me.ObjectEntity.extend(
{

    init: function (x, y, settings)
    {
        //timing
        SKA.lastTime = new Date().getTime();
        SKA.currTime = null;
        SKA.millis = 0;

        // call the constructor
        settings.image = "fish";
        settings.spritewidth = 64;
        settings.spriteheight = 64;
     
        this.parent(x, y, settings);
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since it will be updated manually... might not need it at all
        this.setVelocity(31, 31);
        this.gravity = (SKA.level !== 1) ? SKA.CONSTANTS.BASE_GRAVITY : SKA.CONSTANTS.WATER_GRAVITY;

        this.shotCooldown = 0;
        this.projectile = null;
        this.particle = null;
        this.dir = new me.Vector2d(0, 0);
        this.particlePos = new me.Vector2d(0, 0);
        this.xflip = false;
        this.doubleJumpUsed = false;
        this.charge = 0;
        this.angle = 0;
        // adjust the bounding box
        this.updateColRect(16, 40, 16, 32);
        this.health = SKA.CONSTANTS.PLAYER_MAX_HEALTH;
        this.font = SKA.font32white;
        this.index = 0;
        this.res = null;
        this.damageCooldown = 0;
        this.type = me.game.ACTION_OBJECT;

        // set the display to follow our position on both axis
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
        this.atBoss = false;

        //add this object to the global namespace so others can reference it
        SKA.player = null;
        SKA.player = this;
        SKA.isPaused = false;

        me.game.sort();

        this.renderable.addAnimation("stand", [1, 1, 1, 3, 3]);
        this.renderable.addAnimation("walk", [1, 0, 3, 2]);
        this.renderable.addAnimation("standshoot", [5, 5, 5, 7, 7]);
        this.renderable.addAnimation("walkshoot", [5, 4, 7, 6]);
        this.renderable.addAnimation("jump", [9,9,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]);
        this.renderable.addAnimation("jumpshoot", [11,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]);
    },

    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    update: function ()
    {
        if (SKA.isPaused)
        {
            this.vel.x = 0;
            this.updateMovement();
            this.parent(this);
            return true;
        } else
        {
            if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
            if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;
        }
        //timing
        SKA.currTime = new Date().getTime();
        SKA.millis = SKA.currTime - SKA.lastTime;
        if (SKA.millis > 500) SKA.millis = 16;

        SKA.frames = Math.round(SKA.millis / 16);
        if (SKA.frames === 0) SKA.frames = 1;

        //input
        //movement
        if (me.input.isKeyPressed('right'))
        {
            this.vel.x += SKA.CONSTANTS.PLAYER_SPEED * SKA.frames;
            if (this.vel.x > SKA.CONSTANTS.PLAYER_SPEED * SKA.frames) this.vel.x = SKA.CONSTANTS.PLAYER_SPEED * SKA.frames;
            this.xflip = false;
            this.flipX(false);
        } else if (me.input.isKeyPressed('left'))
        {
            this.vel.x -= SKA.CONSTANTS.PLAYER_SPEED * SKA.frames;
            if (this.vel.x < -SKA.CONSTANTS.PLAYER_SPEED * SKA.frames) this.vel.x = -SKA.CONSTANTS.PLAYER_SPEED * SKA.frames;
            this.xflip = true;
            this.flipX(true);
        } else
        {
            this.vel.x = 0;
        }

        //jumping
        if (!(this.jumping || this.falling))
        {
            this.doubleJumpUsed = false;
            if (me.input.isKeyPressed('up') || me.input.isKeyPressed('space'))
            {
                //jump
                this.vel.y = SKA.CONSTANTS.PLAYER_JUMP;
                this.jumping = true;
            }
        } else if ((me.input.isKeyPressed('up') || me.input.isKeyPressed('space')) && (Math.abs(this.vel.y) <= -.5 * SKA.CONSTANTS.PLAYER_JUMP) && !this.doubleJumpUsed)
        {
            //double jump
            this.doubleJumpUsed = true;
            this.vel.y = SKA.CONSTANTS.PLAYER_JUMP;
            this.jumping = true;
            this.setAnimation('jump', true); //reset animation
        } 
        //this.gravity = SKA.CONSTANTS.BASE_GRAVITY * SKA.frames;

        //shooting
        this.shotCooldown -= SKA.millis;
        if ((me.input.isKeyPressed('shoot1') || me.input.isKeyPressed('shoot2')) && this.shotCooldown <= 0)
        {
            this.shotCooldown = SKA.CONSTANTS.PLAYER_SHOT_COOLDOWN;
            //create a projectile
            if (this.xflip) {
                this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + this.collisionBox.width - SKA.CONSTANTS.PROJECTILE_OFFSET_X, this.pos.y + SKA.CONSTANTS.PROJECTILE_OFFSET_Y, { xvel: -SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0, isPlayer: 1, image: 'projectile' });
            } else {
                this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + SKA.CONSTANTS.PROJECTILE_OFFSET_X, this.pos.y + SKA.CONSTANTS.PROJECTILE_OFFSET_Y, { xvel: SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0, isPlayer: 1, image: 'projectile' });
            }
            me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
            me.game.sort();
        } else if (me.input.isKeyPressed('charge1') || me.input.isKeyPressed('charge2'))
        {
            //handle charge shot
            this.charge += SKA.millis;
            this.vel.x *= .5;
            if (this.charge > SKA.CONSTANTS.PLAYER_CHARGE)
            {
                //charge ready
                this.charge = SKA.CONSTANTS.PLAYER_CHARGE;
            } else
            {
                //spawn animation particle
                if (Math.random() > .3 && SKA.CONSTANTS.PLAYER_CHARGE - this.charge > 300)
                {
                    this.angle = Math.random() * Math.PI * 2;
                    this.particlePos.x = this.pos.x + this.hWidth + SKA.CONSTANTS.CHARGE_PARTICLE_RADIUS * Math.cos(this.angle);
                    this.particlePos.y = this.pos.y + this.hHeight + SKA.CONSTANTS.CHARGE_PARTICLE_RADIUS * Math.sin(this.angle);
                    this.dir.x = this.particlePos.x - this.pos.x - this.hWidth;
                    this.dir.y = this.particlePos.y - this.pos.y - this.hHeight;
                    this.dir.normalize();
                    this.angle = Math.random() * 16;
                    this.particle = me.entityPool.newInstanceOf('particle', this.particlePos.x, this.particlePos.y, {
                        image: 'particle', spritewidth: 8 + this.angle, spriteheight: 8 + this.angle,
                        velX: this.dir.x, velY: this.dir.y,
                        accelX: -.2 * this.dir.x, accelY: -.2 * this.dir.y,
                        duration: 400,
                        r: 128 + Math.floor(Math.random() * 128), g: 64 + Math.floor(Math.random() * 64), b: 0,
                        gravity: 0,
                    });
                    me.game.add(this.particle, SKA.LAYERS.PLAYER);
                    me.game.sort();
                }
            }
        } else
        {
            if (this.charge === SKA.CONSTANTS.PLAYER_CHARGE)
            {
                //shoot charge shot
                if (this.xflip)
                {
                    this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + this.collisionBox.width - SKA.CONSTANTS.PROJECTILE_OFFSET_X, this.pos.y + SKA.CONSTANTS.CHARGE_OFFSET_Y, { xvel: -.7 * SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0, image: 'big_shot', spritewidth: 32, spriteheight: 32, isPlayer: 1, persist: 1 });
                } else
                {
                    this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + SKA.CONSTANTS.PROJECTILE_OFFSET_X, this.pos.y + SKA.CONSTANTS.CHARGE_OFFSET_Y, { xvel: .7 * SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0, image: 'big_shot', spritewidth: 32, spriteheight: 32, isPlayer: 1, persist: 1 });
                }
                me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
                me.game.sort();
                me.game.viewport.shake(5, 15);
            }
            this.charge = 0;
        }

        //set animation
        if (this.jumping || this.falling)
        {
            if ((me.input.isKeyPressed('shoot1') || me.input.isKeyPressed('shoot2')) ||
                ((me.input.isKeyPressed('charge1') || me.input.isKeyPressed('charge2')) && this.charge === SKA.CONSTANTS.PLAYER_CHARGE))
            {
                this.setAnimation('jumpshoot');
            } else
            {
                this.setAnimation('jump');
            }
        } else
        {
            if (this.vel.x !== 0)
            {
                //set to moving
                if ((me.input.isKeyPressed('shoot1') || me.input.isKeyPressed('shoot2')) ||
                ((me.input.isKeyPressed('charge1') || me.input.isKeyPressed('charge2')) && this.charge === SKA.CONSTANTS.PLAYER_CHARGE))
                {
                    this.setAnimation('walkshoot');
                } else
                {
                    this.setAnimation('walk');
                }
            } else
            {
                if ((me.input.isKeyPressed('shoot1') || me.input.isKeyPressed('shoot2')) ||
                ((me.input.isKeyPressed('charge1') || me.input.isKeyPressed('charge2')) && this.charge === SKA.CONSTANTS.PLAYER_CHARGE))
                {
                    this.setAnimation('standshoot');
                } else
                {
                    this.setAnimation('stand');
                }
            }
        }
        this.updateMovement();

        //check for collisions
        if (this.damageCooldown > 0) this.damageCooldown -= SKA.millis;
        this.res = null;
        this.res = me.game.collide(this);
        if (this.res)
        {
            if (this.res.obj.type === me.game.ENEMY_OBJECT && this.damageCooldown <= 0)
            {
                //take damage
                if (this.res.obj.damage) this.health -= this.res.obj.damage;// || SKA.CONSTANTS.SHOT_DAMAGE;

                //bounce back off of enemies
                if (!this.res.obj.damage)
                {
                    this.vel.x = (this.xflip) ? 1 : -1;
                    this.vel.y = -.5;
                    this.vel.normalize();
                    this.vel.x *= SKA.CONSTANTS.PLAYER_BOUNCE_BACK;
                    this.vel.y *= SKA.CONSTANTS.PLAYER_BOUNCE_BACK;
                    this.updateMovement();
                }

                me.game.viewport.shake(7, 30);
                if (this.health <= 0)
                {
                    //die 
                    this.alive = false;
                    SKA.isPaused = true;
                    me.game.viewport.fadeIn('#000000', 1000, SKA.gameOver);
                }
                this.damageCooldown = SKA.CONSTANTS.PLAYER_DAMAGE_COOLDOWN;
                
            }
        }

        //handle level animation
        if (SKA.level === 1)
        {
            if (SKA.bg2) SKA.animationCooldown -= SKA.millis;
            else SKA.animationCooldown += SKA.millis;
            if (SKA.animationCooldown <= 0 || SKA.animationCooldown > SKA.CONSTANTS.ANIMATION)
            {
                SKA.bg2 = !SKA.bg2;
            }
            me.game.currentLevel.getLayerByName('bg2').setOpacity(SKA.animationCooldown / SKA.CONSTANTS.ANIMATION);
            me.game.currentLevel.getLayerByName('bg3').setOpacity(1 - (SKA.animationCooldown / SKA.CONSTANTS.ANIMATION));
        }

        //boss encounter
        if (this.atBoss)
        {
            me.game.viewport.follow(this.pos, me.game.viewport.AXIS.VERTICAL);
            
            //scroll the viewport as far to the right as it will go.
            if (me.game.viewport.pos.x < (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - me.video.getWidth() - 1)
            {
                me.game.viewport.pos.x += SKA.CONSTANTS.PLAYER_SPEED;
                me.game.viewport.pos.x = Math.floor(me.game.viewport.pos.x);
                if (me.game.viewport.pos.x > (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - me.video.getWidth() - 1)
                {
                    me.game.viewport.pos.x = (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - me.video.getWidth() - 1;
                }
                //me.game.viewport.move(me.game.viewport.pos.x + SKA.CONSTANTS.PLAYER_SPEED, me.game.viewport.pos.y);
            }

            //make sure the player doesn't walk off camera
            if (this.pos.x < me.game.viewport.pos.x) this.pos.x = me.game.viewport.pos.x;

        }

        this.parent(this);
        SKA.lastTime = SKA.currTime;
        return true;

    },

    draw: function (context)
    {
        this.parent(context);

        //draw health
        context.fillStyle = "rgba(0,0,0,.5)";
        context.fillRect(me.game.viewport.pos.x, me.game.viewport.pos.y, me.video.getWidth(), 40);
        //health bar
        this.font.draw(context, "Health", 10 + me.game.viewport.pos.x, 4 + me.game.viewport.pos.y);
        for (this.index = 0; this.index < Math.ceil(this.health * .1) ; this.index += 1)
        {
            SKA.font32orange.draw(context, "|", 222 + (14*this.index) + me.game.viewport.pos.x, 4 + me.game.viewport.pos.y);
        }
    }
});

///////////////////////////////////////////////////////////
//PROJECTILE ENTITY
///////////////////////////////////////////////////////////
SKA.ProjectileEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        settings.spritewidth = settings.spritewidth || 16;
        settings.spriteheight = settings.spriteheight || 16;
        settings.image = settings.image || 'enemy_projectile';

        this.direction = new me.Vector2d(settings.xdir, settings.ydir);
        this.direction.normalize();

        this.parent(x, y, settings);
        this.updateColRect(0, settings.spritewidth, .25* settings.spriteheight, .5 * settings.spriteheight);
        this.alwaysUpdate = true;
        this.gravity = 0;
        this.setVelocity(Math.abs(settings.xvel), Math.abs(settings.yvel));
        this.direction.x = settings.xvel;
        this.direction.y = settings.yvel;
        this.vel.x = this.direction.x;
        this.vel.y = this.direction.y;
        this.prevX = this.pos.x;
        this.prevY = this.pos.y;
        this.damage = settings.damage || SKA.CONSTANTS.SHOT_DAMAGE;
        this.isPlayer = settings.isPlayer === 1;
        if (this.isPlayer) this.type = me.game.ACTION_OBJECT;
        else this.type = me.game.ENEMY_OBJECT;

        this.persist = settings.persist;

        if (this.vel.x < 0)
            this.flipX(true);
        if (this.vel.y < 0)
            this.flipY(true);
    },

    update: function ()
    {
        this.parent(this);
        //just update the movement and move along, the collision handler will you know... handle collisions
        this.vel.x = this.direction.x * SKA.frames;
        this.vel.y = this.direction.y * SKA.frames;
        this.updateMovement();
        if (!me.game.viewport.isVisible(this) ||
                this.direction.x !== this.vel.x || this.direction.y !== this.vel.y)
        {
            //kill off
            this.alive = false;
            me.game.remove(this);
        }
        return true;
    },

    onCollision: function (res, obj)
    {
        // remove it
        if ((this.isPlayer && obj.type === me.game.ENEMY_OBJECT && !this.persist) ||
            (!this.isPlayer && obj.type === me.game.ACTION_OBJECT))
        {
            this.alive = false;
            me.game.remove(this);
        }
    }
});

///////////////////////////////////////////////////////////
//  PARTICLE ENTITY
///////////////////////////////////////////////////////////
SKA.ParticleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var w, h, image;
        w = settings.spritewidth;
        h = settings.spriteheight;
        image = me.loader.getImage(settings.image);
        settings.spritewidth = image.width; //correct for when particles are bigger than  image
        settings.spriteheight = image.height;
        this.parent(x, y, settings);
        this.r = settings.r;
        this.g = settings.g;
        this.b = settings.b;
        this.a = 1;
        this.width = w;
        this.height = h;
        this.tweener = new me.Tween(this);
        this.time = 0;
        this.tweener.to({ time: 1 }, settings.duration);
        this.vel.x = settings.velX;
        this.vel.y = settings.velY;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.gravity = settings.gravity || 0;
        this.collidable = false;
        this.float = settings.float === true;

        this.lastViewport = me.game.viewport.pos.x;

        this.tweener.onComplete(this.destroy);
        this.tweener.start();
    },

    update: function ()
    {
        //this.time should be updated by the tweener
        this.vel.x += this.accel.x;
        this.vel.y += this.accel.y;
        //this.renderable.setOpacity(1 - (.5 * this.time));
        //this.renderable.resize(1.5 - this.time);

        this.a = .7 - (.5 * this.time);

        this.updateMovement();

        if (this.float &&
            me.game.viewport.pos.x !== this.lastViewport)
        {
            this.pos.x += me.game.viewport.pos.x - this.lastViewport;
            this.lastViewport = me.game.viewport.pos.x;
        }

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        //context.globalOpacity = this.a;
        context.fillStyle = "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.a + ")";
        context.fillRect(this.pos.x, this.pos.y, this.width, this.height);
        //context.fillRect(this.pos.x + me.game.viewport.pos.x, this.pos.y + me.game.viewport.pos.y, this.width, this.height);
        //context.globalOpacity = 1;
    },

    destroy: function ()
    {
        this.alive = false;
        me.game.remove(this);
    },
});

SKA.spawnParticles = function (amount, dir, spread, x, y, settings)
{

    var unit, i, angle, part, size, vel, baseSize, red, green, blue, dur, variance, baseVel, baseDur, grav, dirVec;
    baseVel = settings.vel || 3.2;
    baseSize = settings.size || 20;
    red = settings.r || 245;
    green = settings.g || 240;
    blue = settings.b || 86;
    variance = settings.variance || 1;
    baseDur = settings.duration || 500;
    grav = settings.gravity || .1;
    dirVec = new me.Vector2d(dir.x, dir.y);
    dirVec.normalize();

    unit = new me.Vector2d(0, 0);
    for (i = 0; i < amount; i++)
    {
        angle = unit.angle(dirVec);
        angle += (.5 * spread) - (spread * Math.random());
        size = 1 + (variance * Math.random());
        vel = baseVel * (2 * (variance * Math.random()));
        dur = baseDur * (1 + (variance * Math.random()));
        part = me.entityPool.newInstanceOf('particle', x, y, {
            image: 'particle', spritewidth: baseSize * (size), spriteheight: baseSize * (size),
            velX: vel * Math.cos(angle), velY: vel * Math.sin(angle),
            accelX: 0, accelY: 0,
            duration: dur,
            r: red, g: green, b: blue,
            gravity: grav,
            float: settings.float
        });
        me.game.add(part, settings.layer);
    }
    me.game.sort();
};

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//  ENEMIES
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  JUMPER ENTITY
///////////////////////////////////////////////////////////
SKA.JumperEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "jumper";
        settings.spritewidth = 32;
        settings.spriteheight = 32;

        this.parent(x, y, settings);
        this.renderable.resize(2);

        this.gravity = (SKA.level !== 1) ? SKA.CONSTANTS.BASE_GRAVITY : SKA.CONSTANTS.WATER_GRAVITY;
        this.collidable = true;

        this.renderable.addAnimation("rest", [0]);
        this.renderable.addAnimation("jump", [1, 2]);

        this.health = SKA.CONSTANTS.JUMPER.MAX_HEALTH;
        this.type = me.game.ENEMY_OBJECT;
        this.jumpCooldown = SKA.CONSTANTS.JUMPER.JUMP_COOLDOWN;
        this.shotCooldown = SKA.CONSTANTS.JUMPER.SHOT_COOLDOWN;
        this.jumping = false;
        this.falling = false;
        this.inAir = false; //used as a flag to determine when it lands on the ground
        this.projectile = null; //memory space for spawning projectiles
        this.shootLeft = true;
        this.alwaysUpdate = true;
        this.type = me.game.ENEMY_OBJECT;

    },

    update: function ()
    {
        //move first to determine if you've just landed
        this.updateMovement();

        //3 action cases
        //1. in air
        if (this.jumping || this.falling)
        {
            this.inAir = true;
            this.shotCooldown -= SKA.millis;
            if (this.shotCooldown <= 0)
            {
                //shoot projectiles
                this.shotCooldown += SKA.CONSTANTS.JUMPER.SHOT_COOLDOWN;
                if (this.shootLeft)
                {
                    this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                        { xvel: -SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0 });
                } else
                {
                    this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                        { xvel: SKA.CONSTANTS.PLAYER_PROJECTILE_SPEED, yvel: 0 });
                }
                this.shootLeft = !this.shootLeft;
                me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
                me.game.sort();
            }
        } else
        {
            if (this.inAir)
            {
                //2. just landed
                this.inAir = false;
                this.jumpCooldown = SKA.CONSTANTS.JUMPER.JUMP_COOLDOWN;
                this.shotCooldown = SKA.CONSTANTS.JUMPER.SHOT_COOLDOWN;
                this.vel.y = 0;
                this.renderable.setCurrentAnimation('rest', 'rest');
            } else
            {
                //3. on ground
                this.jumpCooldown -= SKA.millis;
                if (this.jumpCooldown <= 0)
                {
                    //do jump
                    this.vel.y = SKA.CONSTANTS.JUMPER.VELOCITY * SKA.frames;
                    this.jumping = true;
                    this.inAir = true;
                    this.renderable.setCurrentAnimation('jump', 'jump');
                }
            }
        }


        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        this.parent(context);
        return true;
    }
});

///////////////////////////////////////////////////////////
//  HOPPER ENTITY
///////////////////////////////////////////////////////////
SKA.HopperEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "hopper";
        settings.spritewidth = 32;
        settings.spriteheight = 32;

        this.parent(x, y, settings);
        this.renderable.resize(2);

        this.gravity = (SKA.level !== 1) ? SKA.CONSTANTS.BASE_GRAVITY : SKA.CONSTANTS.WATER_GRAVITY;
        this.collidable = true;

        this.renderable.addAnimation("rest", [0]);
        this.renderable.addAnimation("jump", [1]);

        this.health = SKA.CONSTANTS.HOPPER.MAX_HEALTH;
        this.type = me.game.ENEMY_OBJECT;
        this.jumpCooldown = SKA.CONSTANTS.HOPPER.JUMP_COOLDOWN;
        this.shotCooldown = SKA.CONSTANTS.HOPPER.SHOT_COOLDOWN;
        this.jumping = false;
        this.falling = false;
        this.inAir = false; //used as a flag to determine when it lands on the ground
        this.projectile = null; //memory space for spawning projectiles
        this.shootLeft = true;
        this.alwaysUpdate = false;
        this.dir = new me.Vector2d(0, 0);
        this.type = me.game.ENEMY_OBJECT;

    },

    update: function ()
    {
        //move first to determine if you've just landed
        this.updateMovement();

        this.moveLeft = this.pos.x > SKA.player.pos.x;
        this.flipX(!this.moveLeft);
        //3 action cases
        //1. in air
        if (this.jumping || this.falling)
        {
            this.inAir = true;
        } else
        {
            if (this.inAir)
            {
                //2. just landed
                this.inAir = false;
                this.jumpCooldown = SKA.CONSTANTS.HOPPER.JUMP_COOLDOWN;
                this.vel.y = 0;
                this.vel.x = 0;
                this.renderable.setCurrentAnimation('rest', 'rest');
            } else
            {
                //3. on ground
                this.jumpCooldown -= SKA.millis;
                if (this.jumpCooldown <= 0)
                {
                    //do jump
                    this.vel.y = SKA.CONSTANTS.HOPPER.JUMP_SPEED_Y;
                    if (this.moveLeft)
                    {
                        this.vel.x = -SKA.CONSTANTS.HOPPER.JUMP_SPEED_X * SKA.frames;
                    } else
                    {
                        this.vel.x = SKA.CONSTANTS.HOPPER.JUMP_SPEED_X * SKA.frames;
                    }
                    this.jumping = true;
                    this.inAir = true;
                    this.renderable.setCurrentAnimation('jump', 'jump');
                }
            }
        }

        //shooting
        this.shotCooldown -= SKA.millis;
        if (this.shotCooldown <= 0)
        {
            //shoot projectiles
            this.shotCooldown += SKA.CONSTANTS.HOPPER.SHOT_COOLDOWN;
            this.dir.x = SKA.player.pos.x - this.pos.x;
            this.dir.y = SKA.player.pos.y - this.pos.y;
            this.dir.normalize();
            this.dir.x *= SKA.CONSTANTS.HOPPER.SHOT_SPEED;
            this.dir.y *= SKA.CONSTANTS.HOPPER.SHOT_SPEED;
            this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                { xvel: this.dir.x, yvel: this.dir.y });
            me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
            me.game.sort();
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        this.parent(context);
        return true;
    }
});

///////////////////////////////////////////////////////////
//  SKIPPER ENTITY
///////////////////////////////////////////////////////////
SKA.SkipperEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "skipper";
        settings.spritewidth = 32;
        settings.spriteheight = 64;

        this.parent(x, y, settings);

        if (settings.boss)
        {
            this.renderable.resize(12);
            this.updateColRect(-48, 104, -104, 320);
            this.boss = true;
            this.health = 500;
            this.angle = Math.PI;
            this.angleUp = true;
        } else
        {
            this.renderable.resize(3);
            this.updateColRect(0, 32, 0, 76);
            this.health = SKA.CONSTANTS.SKIPPER.MAX_HEALTH;
        }

        this.gravity = (SKA.level !== 1) ? SKA.CONSTANTS.BASE_GRAVITY : SKA.CONSTANTS.WATER_GRAVITY;
        this.collidable = true;

        this.renderable.addAnimation("rest", [0]);
        this.renderable.addAnimation("jump1", [1]);
        this.renderable.addAnimation("jump2", [2]);

        this.type = me.game.ENEMY_OBJECT;
        this.jumpCooldown = SKA.CONSTANTS.SKIPPER.JUMP_COOLDOWN;
        this.shotCooldown = SKA.CONSTANTS.SKIPPER.SHOT_COOLDOWN;
        this.jumping = false;
        this.falling = false;
        this.inAir = false; //used as a flag to determine when it lands on the ground
        this.projectile = null; //memory space for spawning projectiles
        this.moveLeft = true;
        this.alwaysUpdate = false;
        this.dir = new me.Vector2d(0, 0);
    },

    update: function ()
    {
        if (this.boss)
        {
            this.bossUpdate.call(this);
            return;
        }
        //move first to determine if you've just landed
        this.updateMovement();

        this.moveLeft = this.pos.x > SKA.player.pos.x;
        this.flipX(!this.moveLeft);
        //3 action cases
        //1. in air
        if (this.jumping || this.falling)
        {
            this.inAir = true;
        } else
        {
            if (this.inAir)
            {
                //2. just landed
                this.inAir = false;
                this.jumpCooldown = SKA.CONSTANTS.SKIPPER.JUMP_COOLDOWN;
                this.vel.y = 0;
                this.vel.x = 0;
                this.renderable.setCurrentAnimation('rest', 'rest');
            } else
            {
                //3. on ground
                this.jumpCooldown -= SKA.millis;
                if (this.jumpCooldown <= 0)
                {
                    //do jump
                    this.vel.y = SKA.CONSTANTS.SKIPPER.JUMP_SPEED_Y * SKA.frames;
                    if (this.moveLeft)
                    {
                        this.vel.x = -SKA.CONSTANTS.SKIPPER.JUMP_SPEED_X * SKA.frames;
                    } else
                    {
                        this.vel.x = SKA.CONSTANTS.SKIPPER.JUMP_SPEED_X * SKA.frames;
                    }
                    this.jumping = true;
                    this.inAir = true;
                    if (this.jumpNumber === 1) this.jumpNumber = 2;
                    else this.jumpNumber = 1;
                    this.renderable.setCurrentAnimation('jump' + this.jumpNumber, 'jump' + this.jumpNumber);
                }
            }
        }

        //shooting
        this.shotCooldown -= SKA.millis;
        if (this.shotCooldown <= 0)
        {
            //shoot projectiles
            this.shotCooldown += SKA.CONSTANTS.SKIPPER.SHOT_COOLDOWN;
            this.dir.x = SKA.player.pos.x - this.pos.x;
            this.dir.y = SKA.player.pos.y - this.pos.y;
            this.dir.normalize();
            this.dir.x *= SKA.CONSTANTS.SKIPPER.SHOT_SPEED;
            this.dir.y *= SKA.CONSTANTS.SKIPPER.SHOT_SPEED;
            this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                { xvel: this.dir.x, yvel: this.dir.y });
            me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
            me.game.sort();
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }


        this.parent(this);
        return true;
    },

    bossUpdate: function()
    {
        SKA.player.atBoss = true;

        //1. in air
        if (this.jumping || this.falling)
        {
            this.inAir = true;
        } else
        {
            if (this.inAir)
            {
                //2. just landed
                this.inAir = false;
                this.jumpCooldown = SKA.CONSTANTS.SKIPPER.JUMP_COOLDOWN;
                this.vel.y = 0;
                this.vel.x = 0;
                this.renderable.setCurrentAnimation('rest', 'rest');
            } else
            {
                //3. on ground
                this.jumpCooldown -= SKA.millis;
                if (this.jumpCooldown <= 0)
                {
                    //do jump
                    this.vel.y = SKA.CONSTANTS.SKIPPER.JUMP_SPEED_Y * SKA.frames;
                    this.vel.x = 0;
                    this.jumping = true;
                    this.inAir = true;
                    if (this.jumpNumber === 1) this.jumpNumber = 2;
                    else this.jumpNumber = 1;
                    this.renderable.setCurrentAnimation('jump' + this.jumpNumber, 'jump' + this.jumpNumber);
                }
            }
        }

        //shooting
        this.shotCooldown -= SKA.millis;
        if (this.shotCooldown <= 0)
        {
            //shoot projectiles
            this.shotCooldown += SKA.CONSTANTS.SKIPPER.BOSS_SHOT_COOLDOWN;
            this.angle += (this.angleUp) ? -.05 * Math.PI : .05 * Math.PI;
            if (this.angle >= SKA.CONSTANTS.SKIPPER.BOSS_ANGLE_MAX || this.angle <= SKA.CONSTANTS.SKIPPER.BOSS_ANGLE_MIN) this.angleUp = !this.angleUp;

            this.dir.x = Math.cos(this.angle);
            this.dir.y = Math.sin(this.angle);
            this.dir.x *= SKA.CONSTANTS.SKIPPER.SHOT_SPEED;
            this.dir.y *= SKA.CONSTANTS.SKIPPER.SHOT_SPEED;
            this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + -.3 * this.collisionBox.width, this.pos.y + -.25 * this.collisionBox.height,
                { xvel: this.dir.x, yvel: this.dir.y });
            me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
            me.game.sort();
        }

        this.updateMovement();

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(20, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 32, r: 255, g: 255, b: 85, duration: 400, vel: 20, variance: .1 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;

                        SKA.isPaused = true;
                        //me.game.remove(this);
                        me.game.viewport.fadeIn('#000000', 1000, SKA.goToNextLevel);
                    }
                }
            }
        }

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        this.parent(context);
        if (this.alive && SKA.player.atBoss)
        {
            SKA.font32white.draw(context, "Boss", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 672,
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 64)
            for (this.index = 0; this.index < Math.ceil(this.health * .05) ; this.index += 1)
            {
                SKA.font32orange.draw(context, "|", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 544 + (14 * this.index),
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 60);
            }
        }
        return true;
    }
});

///////////////////////////////////////////////////////////
//  BUZZER ENTITY
///////////////////////////////////////////////////////////
SKA.BuzzerEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "buzzer";
        settings.spritewidth = 32;
        settings.spriteheight = 32;

        this.parent(x, y, settings);
        this.renderable.resize(2);
        //this.updateColRect(0, 28, 8, 16);

        this.toad = settings.toad || null;

        this.gravity = 0;
        this.collidable = true;

        this.renderable.addAnimation("fly", [0, 1]);
        this.renderable.setCurrentAnimation('fly', 'fly');

        this.health = SKA.CONSTANTS.BUZZER.MAX_HEALTH;
        this.type = me.game.ENEMY_OBJECT;
        this.shotCooldown = SKA.CONSTANTS.BUZZER.SHOT_COOLDOWN;
        this.jumping = false;
        this.falling = false;
        this.inAir = false; //used as a flag to determine when it lands on the ground
        this.projectile = null; //memory space for spawning projectiles
        this.shootLeft = true;
        this.alwaysUpdate = true;
        this.dir = new me.Vector2d(0, 0);
        this.target = new me.Vector2d(settings.targetx || this.pos.x, settings.targety || this.pos.y);
        this.rotationCenter = new me.Vector2d(this.pos.x, this.pos.y);
        this.rotationAngle = Math.random() * 2 * Math.PI;
        this.radius = SKA.CONSTANTS.BUZZER.RADIUS_MIN + (Math.random() * SKA.CONSTANTS.BUZZER.RADIUS_MAX - SKA.CONSTANTS.BUZZER.RADIUS_MIN);
    },

    update: function ()
    {
        //add some movement
        if (this.rotationCenter.distance(this.target) < SKA.CONSTANTS.BUZZER.VELOCITY)
        {
            this.rotationCenter.x = this.target.x;
            this.rotationCenter.y = this.target.y;
            this.vel.x = 0;
            this.vel.y = 0;
        } else
        {
            this.vel.x = this.target.x - this.rotationCenter.x;
            this.vel.y = this.target.y - this.rotationCenter.y;

            if (this.vel.length() > SKA.CONSTANTS.BUZZER.VELOCITY)
            {
                this.vel.normalize();
                this.vel.x *= SKA.CONSTANTS.BUZZER.VELOCITY * SKA.frames;
                this.vel.y *= SKA.CONSTANTS.BUZZER.VELOCITY * SKA.frames;
            }
        }
        this.rotationCenter.x += this.vel.x;
        this.rotationCenter.y += this.vel.y;
        this.rotationAngle += SKA.CONSTANTS.BUZZER.ROTATION_RATE;
        this.pos.x = this.rotationCenter.x + this.radius * Math.cos(this.rotationAngle);
        this.pos.y = this.rotationCenter.y + this.radius * Math.sin(this.rotationAngle);



        this.moveLeft = this.pos.x > SKA.player.pos.x;
        this.flipX(!this.moveLeft);

        //shooting
        this.shotCooldown -= SKA.millis;
        if (this.shotCooldown <= 0)
        {
            //shoot projectiles
            this.shotCooldown += SKA.CONSTANTS.BUZZER.SHOT_COOLDOWN;
            this.dir.x = SKA.player.pos.x - this.pos.x;
            this.dir.y = SKA.player.pos.y - this.pos.y;
            this.dir.normalize();
            this.dir.x *= SKA.CONSTANTS.BUZZER.SHOT_SPEED;
            this.dir.y *= SKA.CONSTANTS.BUZZER.SHOT_SPEED;
            this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                { xvel: this.dir.x, yvel: this.dir.y });
            me.game.add(this.projectile, SKA.LAYERS.PARTICLES);
            me.game.sort();
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        if (this.toad) this.toad.buzzCount -= 1;
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }


        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        this.parent(context);
        return true;
    }
});

///////////////////////////////////////////////////////////
//  HIVE TOAD ENTITY
///////////////////////////////////////////////////////////
SKA.HiveToadEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "hive_toad";
        settings.spritewidth = 32;
        settings.spriteheight = 32;

        this.parent(x, y, settings);

        if (settings.boss)
        {
            this.renderable.resize(6);
            this.updateColRect(-24, 56, -24, 132);
            this.boss = true;
            this.health = 500;
        } else
        {
            this.renderable.resize(2);
            this.updateColRect(-8, 56, -8, 56);
            this.health = SKA.CONSTANTS.HIVE_TOAD.MAX_HEALTH;
        }


        this.gravity = (SKA.level !== 1) ? SKA.CONSTANTS.BASE_GRAVITY : SKA.CONSTANTS.WATER_GRAVITY;
        this.collidable = true;

        this.renderable.addAnimation("sit", [0,0,0, 1,1]);
        this.renderable.setCurrentAnimation('sit', 'sit');

        this.type = me.game.ENEMY_OBJECT;
        this.health = SKA.CONSTANTS.HIVE_TOAD.MAX_HEALTH;
        this.cooldown = SKA.CONSTANTS.HIVE_TOAD.FULL_COOLDOWN;
        this.jumping = false;
        this.falling = false;
        this.buzz = null; //memory space for spawning buzzers
        this.shootLeft = true;
        this.alwaysUpdate = false;
        this.buzzCount = 0;
        this.buzzMax = settings.buzzMax || SKA.CONSTANTS.HIVE_TOAD.BUZZERS_MAX;
        this.target = new me.Vector2d(0,0);
    },

    update: function ()
    {
        if (this.boss)
        {
            this.bossUpdate.call(this);
            return;
        }
        this.moveLeft = this.pos.x > SKA.player.pos.x;
        this.flipX(!this.moveLeft);

        this.updateMovement();

        //spawning
        if (this.buzzCount < this.buzzMax)
        {
            this.cooldown -= SKA.millis;
            if (this.cooldown <= 0)
            {
                //shoot projectiles
                this.buzzCount += 1;
                this.cooldown += this.buzzCount === this.buzzMax ? SKA.CONSTANTS.HIVE_TOAD.FULL_COOLDOWN : SKA.CONSTANTS.HIVE_TOAD.SHORT_COOLDOWN;
                this.target.x = me.game.viewport.pos.x + (.2 * me.game.viewport.width) + (Math.random() * .6 * me.game.viewport.width);
                this.target.y = me.game.viewport.pos.y + (.2 * me.game.viewport.height) + (Math.random() * .6 * me.game.viewport.height);
                this.buzz = me.entityPool.newInstanceOf('buzzer', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                    { targetx: this.target.x, targety: this.target.y, toad:this });
                me.game.add(this.buzz, SKA.LAYERS.PARTICLES);
                me.game.sort();
            }
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }


        this.parent(this);
        return true;
    },

    bossUpdate: function()
    {
        if (!this.alive)
        {
            SKA.spawnParticles(20, { x: 0, y:1 }, 2 * Math.PI,
                this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 32, r: 255, g: 255, b: 85, duration: 400, vel: 20, variance: .1 });
        }

        //make sure the player knows he's at the boss
        SKA.player.atBoss = true;


        this.updateMovement();

        //spawning
        if (this.buzzCount < this.buzzMax)
        {
            this.cooldown -= SKA.millis;
            if (this.cooldown <= 0)
            {
                //shoot projectiles
                this.buzzCount += 1;
                this.cooldown += this.buzzCount === this.buzzMax ? SKA.CONSTANTS.HIVE_TOAD.FULL_COOLDOWN : SKA.CONSTANTS.HIVE_TOAD.SHORT_COOLDOWN;
                this.target.x = me.game.viewport.pos.x + (.2 * me.game.viewport.width) + (Math.random() * .6 * me.game.viewport.width);
                this.target.y = me.game.viewport.pos.y + (.2 * me.game.viewport.height) + (Math.random() * .6 * me.game.viewport.height);
                this.buzz = me.entityPool.newInstanceOf('buzzer', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                    { targetx: this.target.x, targety: this.target.y, toad: this });
                me.game.add(this.buzz, SKA.LAYERS.PARTICLES);
                me.game.sort();
            }
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(20, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x, this.pos.y,
                        { layer: SKA.LAYERS.PARTICLES, size: 32, r: 255, g: 255, b: 85, duration: 400, vel: 20, variance: .1 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;

                        SKA.isPaused = true;
                        //me.game.remove(this);
                        me.game.viewport.fadeIn('#000000', 1000, SKA.goToNextLevel);
                    }
                }
            }
        }
    },

    draw: function (context)
    {
        this.parent(context);
        if (this.alive && SKA.player.atBoss)
        {
            SKA.font32white.draw(context, "Boss", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 672,
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 64)
            for (this.index = 0; this.index < Math.ceil(this.health * .05) ; this.index += 1)
            {
                SKA.font32orange.draw(context, "|", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 544 + (14 * this.index),
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 60);
            }
        }
        return true;
    }
});


///////////////////////////////////////////////////////////
//  SWIMMER ENTITY
///////////////////////////////////////////////////////////
SKA.SwimmerEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "swimmer";
        settings.spritewidth = 32;
        settings.spriteheight = 32;

        this.parent(x, y, settings);

        if (settings.boss)
        {
            this.renderable.resize(8);
            this.updateColRect(-32, 128, -48, 128);
            this.boss = true;
            this.health = 500;
        } else
        {
            this.renderable.resize(2);
            this.updateColRect(0, 28, 4, 24);
            this.health = SKA.CONSTANTS.SWIMMER.MAX_HEALTH;
        }
        this.type = me.game.ENEMY_OBJECT;

        this.gravity = 0;
        this.collidable = true;

        this.renderable.addAnimation("swim", [0, 0, 0, 0, 0, 1, 1]);
        this.renderable.addAnimation("shoot", [2]);
        this.renderable.setCurrentAnimation('swim', 'swim');

        this.shotCooldown = SKA.CONSTANTS.SWIMMER.FULL_COOLDOWN;
        this.jumping = false;
        this.falling = false;

        this.projectile = null; //memory space for spawning projectiles
        this.moveLeft = true;
        this.alwaysUpdate = true;

        this.startX = x + settings.width;
        this.endX = x;
        this.yvar = 0;
        this.ybase = y;

        this.shotsToFire = 0;
        this.shotAngle = 0;

        if (this.boss)
        {
            this.alwaysUpdate = false;
            this.index = 0;
        }

        this.dir = new me.Vector2d(0, 0);
        this.i = 0;
    },

    update: function ()
    {
        if (this.boss)
        {
            this.bossUpdate.call(this);
            return;
        }
        if (this.shotsToFire > 0)
        {
            //firing mode
            //shooting
            this.shotCooldown -= SKA.millis;
            if (this.shotCooldown <= 0)
            {
                //shoot projectiles
                this.shotsToFire -= 1;
                this.shotCooldown += (this.shotsToFire > 0) ? SKA.CONSTANTS.SWIMMER.SHORT_COOLDOWN : SKA.CONSTANTS.SWIMMER.FULL_COOLDOWN;

                for (this.i = 0; this.i < 4; this.i += 1)
                {
                    this.dir.x = Math.cos(this.shotAngle);
                    this.dir.y = Math.sin(this.shotAngle);
                    this.dir.x *= SKA.CONSTANTS.SWIMMER.SHOT_SPEED;
                    this.dir.y *= SKA.CONSTANTS.SWIMMER.SHOT_SPEED;
                    this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x + .5 * this.collisionBox.width, this.pos.y + .5 * this.collisionBox.height,
                        { xvel: this.dir.x, yvel: this.dir.y });
                    this.shotAngle += .5 * Math.PI;

                    me.game.add(this.projectile, SKA.LAYERS.PLAYER);
                    me.game.sort();
                }

                this.shotAngle = (SKA.CONSTANTS.SWIMMER.SHOTS - this.shotsToFire) * Math.PI * .166667;
            }

        } else
        {
            //patrol mode
            this.shotCooldown -= SKA.millis;
            if (this.shotCooldown <= 0)
            {

                if (!this.renderable.isCurrentAnimation('swim')) this.renderable.setCurrentAnimation('swim', 'swim');

                //add some movement
                if (this.moveLeft)
                {
                    this.vel.x = -SKA.CONSTANTS.SWIMMER.VELOCITY * SKA.frames;
                } else
                {
                    this.vel.x = SKA.CONSTANTS.SWIMMER.VELOCITY * SKA.frames;
                }
                this.flipX(this.vel.x > 0);
                this.updateMovement();
                this.pos.y = this.ybase + SKA.CONSTANTS.SWIMMER.Y_VARIANCE * Math.sin(SKA.currTime * .01 / Math.PI);
                if (this.pos.x > this.startX || this.pos.x < this.endX) this.moveLeft = !this.moveLeft;

                //see if the player is in range to start shooting
                if (Math.abs(this.pos.x + this.hWidth - SKA.player.pos.x - SKA.player.hWidth) <= SKA.CONSTANTS.SWIMMER.ATTACK_RANGE)
                {
                    this.shotsToFire = SKA.CONSTANTS.SWIMMER.SHOTS;
                    this.shotCooldown = 0;
                    this.shotAngle = 0;
                    this.renderable.setCurrentAnimation('shoot', 'shoot');
                }
            }
        }

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(8, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 8, r: 255, g: 255, b: 85, duration: 300, vel: .8, variance: 0 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        me.game.remove(this);
                    }
                }
            }
        }

        this.parent(this);
        return true;
    },

    bossUpdate: function()
    {
        if (!this.alive)
        {
            SKA.spawnParticles(20, { x: 0, y:1 }, 2 * Math.PI,
                this.pos.x + this.hWidth - 4, this.pos.y + this.hHeight - 4,
                        { layer: SKA.LAYERS.PARTICLES, size: 32, r: 255, g: 255, b: 85, duration: 400, vel: 20, variance: .1 });
            }

        //make sure the player knows he's at the boss
        SKA.player.atBoss = true;

        if (!this.renderable.isCurrentAnimation('shoot')) this.renderable.setCurrentAnimation('shoot', 'shoot');

        this.flipX(false);

        this.pos.y = this.ybase + SKA.CONSTANTS.SWIMMER.Y_VARIANCE * Math.sin(SKA.currTime * .01 / Math.PI);

        this.shotCooldown -= SKA.millis;
        if (this.shotCooldown <= 0)
        {
            //shoot projectiles
            this.shotsToFire -= 1;
            this.shotCooldown += (this.shotsToFire > 0) ? .25 * SKA.CONSTANTS.SWIMMER.SHORT_COOLDOWN : SKA.CONSTANTS.SWIMMER.FULL_COOLDOWN;

            this.dir.x = SKA.player.pos.x - this.pos.x;
            this.dir.y = SKA.player.pos.y - this.pos.y + .5 * this.hHeight;
            this.dir.normalize();
            this.dir.x *= SKA.CONSTANTS.SWIMMER.SHOT_SPEED;
            this.dir.y *= SKA.CONSTANTS.SWIMMER.SHOT_SPEED;

            this.projectile = me.entityPool.newInstanceOf('projectile', this.pos.x, this.pos.y + .5 * this.hHeight,
                                                        { xvel: this.dir.x, yvel: this.dir.y });
            me.game.add(this.projectile, SKA.LAYERS.PLAYER);
            me.game.sort();

        }
        if (this.shotsToFire === 0)
        {
            this.shotsToFire = 2 * SKA.CONSTANTS.SWIMMER.SHOTS;
        }

        this.updateMovement();

        //check for collisions
        this.res = null;
        this.res = me.game.collide(this, true);
        if (this.res.length > 0)
        {
            for (this.index = 0; this.index < this.res.length; this.index += 1)
            {
                if (this.res[this.index].obj.type === me.game.ACTION_OBJECT && this.res[this.index].obj.isPlayer)
                {
                    //take damage
                    this.health -= this.res[this.index].obj.damage;
                    SKA.spawnParticles(20, { x: -this.res[this.index].x, y: -this.res[this.index].y }, .7 * Math.PI,
                        this.pos.x, this.pos.y,
                        { layer: SKA.LAYERS.PARTICLES, size: 32, r: 255, g: 255, b: 85, duration: 400, vel:20, variance: .1 });
                    if (this.health <= 0)
                    {
                        //die 
                        this.alive = false;
                        
                        SKA.isPaused = true;
                        //me.game.remove(this);
                        me.game.viewport.fadeIn('#000000', 1000, SKA.goToNextLevel);
                    }
                }
            }
        }

    },

    draw: function (context)
    {
        this.parent(context);
        if (this.alive && SKA.player.atBoss)
        {
            SKA.font32white.draw(context, "Boss", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 672 ,
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 64)
            for (this.index = 0; this.index < Math.ceil(this.health * .05) ; this.index += 1)
            {
                SKA.font32orange.draw(context, "|", (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - 544 + (14 * this.index),
                    (me.game.currentLevel.rows * me.game.currentLevel.tileheight) - 60);
            }
        }
        return true;
    }
});

SKA.goToNextLevel = function ()
{
    if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
    if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;
    if (SKA.level < 3)
    {
        //next level
        SKA.level += 1;
        me.state.change(me.state.USER + SKA.level);
    } else
    {
        //game over, you win
        me.state.change(me.state.GAME_END);
    }
}

SKA.gameOver = function ()
{
    if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
    if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;
    me.state.change(me.state.GAMEOVER);
}