/* Game namespace */
var SKA = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        //if (!me.video.init("screen", 960, 640, true, 'auto'))
            //if (!me.video.init("screen", 480, 320, true, 'auto'))
        if (!me.video.init("screen", 720, 480, true, 'auto'))
        {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(SKA.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded" : function () {
        me.state.set(me.state.MENU, new SKA.TitleScreen());
        me.state.set(me.state.PLAY, new SKA.PlayScreen());
        me.state.set(me.state.CREDITS, new SKA.InfoScreen('credits_bg', me.state.MENU));
        me.state.set(me.state.GAMEOVER, new SKA.InfoScreen('game_over', me.state.MENU));
        me.state.set(me.state.GAME_END, new SKA.InfoScreen('game_end', me.state.CREDITS));
        me.state.set(me.state.USER, new SKA.LevelStartScreen(1, 'level1_bg', 'level1_slide', 'level1_snap1', 'level1_snap2', 'level1_snap3'));
        me.state.set(me.state.USER + 2, new SKA.LevelStartScreen(2, 'level2_bg', 'level2_slide', 'level2_snap1', 'level2_snap2', 'level2_snap3'));
        me.state.set(me.state.USER + 3, new SKA.LevelStartScreen(3, 'level3_bg', 'level3_slide', 'level3_snap1', 'level3_snap2', 'level3_snap3'));

        //global fonts
        SKA.font32white = new me.BitmapFont('font32white', 32);
        SKA.font32orange = new me.BitmapFont('font32orange', 32);
        SKA.font16white = new me.BitmapFont('font32white', 32);
        SKA.font16white.resize(.5);

        //input buttons
        me.input.bindKey(me.input.KEY.UP, "up", true);
        me.input.bindKey(me.input.KEY.DOWN, "down");
        me.input.bindKey(me.input.KEY.LEFT, "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.ENTER, "enter", true);
        me.input.bindKey(me.input.KEY.ESCAPE, "escape", true);
        me.input.bindKey(me.input.KEY.SPACE, "space", true);
        me.input.bindKey(me.input.KEY.X, "shoot1");
        me.input.bindKey(me.input.KEY.J, "shoot2");
        me.input.bindKey(me.input.KEY.C, "charge1");
        me.input.bindKey(me.input.KEY.K, "charge2");

        //pooled entities
        me.entityPool.add("player", SKA.PlayerEntity);
        me.entityPool.add("projectile", SKA.ProjectileEntity);
        me.entityPool.add("particle", SKA.ParticleEntity);
        me.entityPool.add("jumper", SKA.JumperEntity);
        me.entityPool.add("hopper", SKA.HopperEntity);
        me.entityPool.add("skipper", SKA.SkipperEntity);
        me.entityPool.add("buzzer", SKA.BuzzerEntity);
        me.entityPool.add("hive_toad", SKA.HiveToadEntity);
        me.entityPool.add("swimmer", SKA.SwimmerEntity);

        //me.debug.renderHitBox = true;
        //me.debug.renderCollisionMap = true;

        // Start the game.
        me.state.change(me.state.MENU);
    }
};
