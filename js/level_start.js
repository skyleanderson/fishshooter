﻿SKA = SKA || {};

//constants
SKA.snap1x = 550;
SKA.snap1y = 40;
SKA.snap2x = 450;
SKA.snap2y = 170;
SKA.snap3x = 350;
SKA.snap3y = 300;
SKA.snapTimeout = 500;

SKA.LevelStartScreen = me.ScreenObject.extend(
{
    init: function (level, bg, slide, snap1, snap2, snap3)
    {
        this.parent(true, true);
        this.bg = me.loader.getImage(bg);;
        this.slide = me.loader.getImage(slide);
        this.snap1 = me.loader.getImage(snap1);
        this.snap2 = me.loader.getImage(snap2);
        this.snap3 = me.loader.getImage(snap3);

        this.level = level;

        this.show1 = false;
        this.show2 = false;
        this.show3 = false;

        this.slidePosition = -me.video.getWidth();

        this.tweener = new me.Tween(this);
    },

    onResetEvent: function ()
    {
        var self = this;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;

        this.slidePosition = -me.video.getWidth();

        this.tweener.to({ slidePosition: 0 }, 1000);
        this.tweener.easing(me.Tween.Easing.Cubic.InOut);
        this.tweener.onComplete(function () { self.timeout1.call(self); });
        this.tweener.start();
    },

    //timeout callback to snap images in place
    timeout1: function ()
    {
        var self = this;
        self.show1 = true;
        //play sound?
        setTimeout(function () { self.timeout2.call(self); }, SKA.snapTimeout);
    },

    timeout2: function ()
    {
        var self = this;
        self.show2 = true;
        //play sound?
        setTimeout(function () { self.timeout3.call(self); }, SKA.snapTimeout);
    },

    timeout3: function ()
    {
        var self = this;
        self.show3 = true;
        //play sound?
        setTimeout(function ()
        {
            SKA.level = self.level;
            me.state.change(me.state.PLAY);
        }, 7 * SKA.snapTimeout);
    },

    update: function ()
    {
        this.parent();
        return true;
    },

    draw: function (context)
    {
        //bg
        context.drawImage(this.bg, 0, 0, me.video.getWidth(), me.video.getHeight());

        //slide
        context.drawImage(this.slide, this.slidePosition, 0, me.video.getWidth(), me.video.getHeight());

        //snaps
        if (this.show1) { context.drawImage(this.snap1, SKA.snap1x, SKA.snap1y, 150, 150); }
        if (this.show2) { context.drawImage(this.snap2, SKA.snap2x, SKA.snap2y, 150, 150); }
        if (this.show3) { context.drawImage(this.snap3, SKA.snap3x, SKA.snap3y, 150, 150); }
    }

});