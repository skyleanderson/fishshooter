SKA = SKA || {};

SKA.TitleScreen = me.ScreenObject.extend(
{
    /*
    * constructor
    */
    init: function ()
    {
        // call parent constructor
        this.parent(true, true);

        // init stuff
        this.background = null;
        this.play = null;
        this.credits = null;
        this.version = null;
        this.selection = null;

       
        SKA.titleIndex = 0;
    },


    /*
    * reset function
    */
    onResetEvent: function ()
    {
        var characterTween;
        // load title image
        this.background = me.loader.getImage("title_bg");
        this.selection = me.loader.getImage("title_select");
        this.character = new me.ObjectEntity(100, 120, { image: "fish_big" });
        this.character.renderable.resize(2);

        // play button
        this.play = new SKA.Button(0, "play", me.state.USER, .38 * me.video.getWidth(), 270);
        this.credits = new SKA.Button(1, "credits", me.state.CREDITS, .38 * me.video.getWidth(), 344);

        //            this.mute = new MuteButton(me.video.getWidth() - 35, me.video.getHeight() - 35); 

        // version
        this.version = new me.BitmapFont("font32white", 32);
        this.version.resize(.5);
        me.game.sort();

        this.ready = false;
        this.bgPosition = -me.video.getWidth();
        this.characterPosition = -256;
        this.menuPosition = me.video.getHeight();

        this.titleTween = new me.Tween(this);
        this.titleTween.to({ bgPosition: 0 }, 1000);
        this.titleTween.easing(me.Tween.Easing.Cubic.InOut);

        this.characterTween = new me.Tween(this);
        this.characterTween.to({ characterPosition: 120 }, 1000);
        this.characterTween.easing(me.Tween.Easing.Bounce.Out);

        this.menuTween = new me.Tween(this);
        this.menuTween.to({ menuPosition: 270 }, 500);
        this.menuTween.easing(me.Tween.Easing.Cubic.InOut);

        this.menuTween.onComplete(function ()
        {
            this.ready = true;
        });

        this.characterTween.chain(this.menuTween);
        characterTween = this.characterTween;
        //titleTween.chain(characterTween);
        setTimeout(function () { characterTween.start(); }, 800);
        this.titleTween.start();
    },

    /**
     * update function
     */
    update: function ()
    {
        this.parent();

        if (this.ready)
        {

            if (me.input.isKeyPressed("down"))
                SKA.titleIndex = 1;
            else if (me.input.isKeyPressed("up"))
                SKA.titleIndex = 0;


            //check if the mouse is hovered
            if (this.play.containsPoint(me.input.mouse.pos))
            {
                SKA.titleIndex = 0;
            } else if (this.play.containsPoint(me.input.mouse.pos))
            {
                SKA.titleIndex = 1;
            }

            if (me.input.isKeyPressed("enter"))
            {
                if (SKA.titleIndex === 0)
                {
                    SKA.level = 1;
                    me.state.change(me.state.USER);
                }
                else
                    me.state.change(me.state.CREDITS);

            }
        } else
        {
            this.play.pos.y = this.menuPosition;
            this.credits.pos.y = this.menuPosition + 74;
            this.character.pos.y = this.characterPosition;
        }
        return true;
    },

    /*
    * drawing function
    */
    draw: function (context)
    {
        //clearScreen
        context.fillStyle = "rgb(0,0,0)";
        context.fillRect(0, 0, me.video.getWidth(), me.video.getHeight());

        // draw title
        context.drawImage(this.background, this.bgPosition, 0, me.video.getWidth(), me.video.getHeight());

        //draw character
        this.character.draw(context);

        //draw selection indicator
        context.drawImage(this.selection, 0, this.play.pos.y + (74 * SKA.titleIndex));

        // draw play button
        this.play.draw(context);
        this.credits.draw(context);

        //            this.mute.draw(context);
        var versionText = "v1.0 #skyleanderson 2013";
        var versionSize = this.version.measureText(context, versionText);
        this.version.draw(context, versionText,
            me.video.getWidth() - versionSize.width, me.video.getHeight() - versionSize.height - 16);

        //test
        //SKA.font16white.draw(context, "Bitmap, g ^_^ Font Test12!@[]", 0, me.video.getHeight() - 200);
    },

    /*
    * destroy event function
    */
    onDestroyEvent: function ()
    {
        // release mouse event
        me.input.releasePointerEvent("mousedown", this.play);
        me.input.releasePointerEvent("mousedown", this.credits);
    }
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
SKA.Button = me.Rect.extend(
{
    /*
    * constructor
    */
    init: function (index, image, action, x, y)
    {
        // init stuff
        this.index = index;
        this.image = me.loader.getImage(image);
        this.action = action;

        //if x or y is -1, center on screen
        var endx = x >= 0 ? x : (me.video.getWidth() / 2 - this.image.width / 2);
        var endy = y >= 0 ? y : (me.video.getHeight() / 2 - this.image.height / 2);

        this.pos = new me.Vector2d(endx, endy);

        // call parent constructor
        this.parent(this.pos, this.image.width, this.image.height);

        // register mouse event
        me.input.registerPointerEvent("mousedown", this, this.clicked.bind(this));
    },

    /*
    * action to perform when a button is clicked
    */
    clicked: function ()
    {
        // start action
        SKA.level = 1;
        me.state.change(this.action);
    },

    /*
    * drawing function
    */
    draw: function (context)
    {
        context.drawImage(this.image, this.pos.x, this.pos.y);
    },

    update: function ()
    {

        this.parent();
        return true;
    },

    /*
    * destroy event function
    */
    onDestroyEvent: function ()
    {
        // release mouse events
        me.input.releasePointerEvent("mousedown", this);
    }
});
